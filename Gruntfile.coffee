module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    watch:
      doc:
        files: ['doc/src/*.md']
        tasks: ['aglio']

    aglio:
      doc:
        files:
          "doc/dist/api.html": ["doc/src/*.md"]
        options:
          separator: "\n"

  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-aglio')