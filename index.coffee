lightController = require './src/lights'
restify = require 'restify'

server = restify.createServer
	name: 'Lights App'

server.use(restify.bodyParser())
server.use(restify.jsonp())
server.use(restify.queryParser())

server.get '/status', (req, res, next) ->
	response =
		timeout: lightController.getTimeout()
		channels: lightController.getChannels()
	res.send(200, response)
	return next()

server.post '/level', (req, res, next) ->
	channel = req.params.channel
	level = req.params.level

	lightController.setChannelLevel channel, level

	response = {}
	response[channel] = level

	res.send(200, response)
	return next()

server.get '/level', (req, res, next) ->
	channel = req.params.channel
	level = req.params.level

	channels = lightController.getChannels()
	response = {}
	response[channel] = channels[channel]

	console.log response

	res.send(200, response)
	return next()

server.post '/timeout', (req, res, next) ->
	seconds = req.params.seconds

	lightController.setTimeout(parseInt(seconds))

	res.send(200, {'timeout': lightController.getTimeout()})
	return next()

server.get '/timeout', (req, res, next) ->
	res.send(200, {'timeout': lightController.getTimeout()})
	return next()

server.get /\/?.*/, restify.serveStatic
	directory: './doc/dist',
	default: 'index.html'

server.listen 7070
