rtpmidi = require('rtpmidi')

channels =
  general: 0
  ch1: 0
  ch2: 0
  ch3: 0
  ch4: 0
  ch5: 0
  ch6: 0
  ch7: 0
  ch8: 0
  ch9: 0
  ch10: 0

channels_mapping =
  general: 16
  ch1: 0
  ch2: 1
  ch3: 2
  ch4: 3
  ch5: 4
  ch6: 5
  ch7: 6
  ch8: 7
  ch9: 8
  ch10: 9

session = rtpmidi.manager.createSession
  localName: 'Node process'
  bonjourName: 'Node RTPMidi'
  port: 5006

session.on 'ready', ->
  console.log 'ready'

session.on 'message', (deltaTime, message) ->
  console.log "Receive message on #{deltaTime}: #{message}"

sendMessage = (command, level) ->
  session.sendMessage [0x90, command, level]
  session.sendMessage [0x80, command, level]
  session.sendMessage [0xB0, command, level]
  # session.sendMessage [272, command, level]

exports.getChannels = ->
  channels

exports.setChannelLevel = (channel, level) ->
  channels[channel] = level % 128
  sendMessage(channels_mapping[channel], channels[channel])

timeEvent = -1
timeoutId = null

exports.setTimeout = (seconds) ->
  clearTimeout(timeoutId)

  if seconds >= 0
    t = Date.now()
    timeEvent = t/1000 + seconds

    timeoutId = setTimeout ->
      exports.setChannelLevel("general", 0)
    , seconds * 1000

exports.getTimeout = ->
  if timeEvent >= 0
    currentSeconds = (Date.now())/1000
    if (currentSeconds < timeEvent)
      return Math.floor(timeEvent - currentSeconds)

  return -1

module.exports = exports
