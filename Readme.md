Lights API REST
===============

API for controlling a set of lights through a RPCMIDI Controller

Install: 

	libavahi-client-dev
	libavahi-compat-libdnssd-dev
	libasound2-dev

The full documentation of the API has been generated with APIAry and it's available on [Doc folder](./doc/dist/api.html)