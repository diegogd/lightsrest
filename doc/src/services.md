FORMAT: 1A

# REST Lights API
This API allow controlling the lights of Radmas' office.

The lights are mapped as follow:

- Panels (lights over computers):

| ch5 | ch6 | ch5 | ch6 |
|-----|-----|-----|-----|
| ch1 | ch2 | ch3 | ch4 |

- Lights back

        ch7 ch8
        ch9

- Lights room:

        ch10


## Status [/status]

This request gives information about the current state of the lights.

- timeout: stands for the remaining time until the general level transits to 0.

+ Model (application/json)
    + Body
        {
            "timeout": -1,
            "channels": {
                "general": 127,
                "ch1": 0,
                "ch2": 127,
                "ch3": 127,
                "ch4": 127,
                "ch5": 127,
                "ch6": 127,
                "ch7": 127,
                "ch8": 127,
                "ch9": 127,
                "ch10": 127
            }
        }

### Get current state [GET]

+ Response 200
    [Status][]

## Set Levels [/level]

This service allow changing the level of some channels.

+ Parameters
    + channel (required, string) ... Any of the available channels.
    + value (required, int) ... Value from 0 to 127

### Set values [POST]

### Get values [GET]

+ Parameters
    + channel (required, string) ... Any of the available channels.

## Timeout [/timeout]

This service set the general channel to 0 in a given amount of time.



+ Model
    + Body
        {
            "timeout": 300
        }

### Set timeout [POST]

+ Parameters
    + seconds (required, int) ... Number of seconds until the general channel is set to 0. This value can be set to -1.

+ Response 200
    [Timeout][]

### Get timeout [GET]

+ Response 200
    [Timeout][]

